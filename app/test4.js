export default function getPrimes(max){
/*
Modify the contents of 'app/test4.js' and implement a function that will return an array of prime numbers.

https://en.wikipedia.org/wiki/Prime_number

The function must take an number argument and generate all the prime numbers that appear BEFORE the argument.
*/

var array = new Array();
if(max == 0 || max == null || max == 1 || max ==2){
	return array; //if the max is below 0 or null return blank array 
}
else{
	array.push(2); //add 2 to array as it is prime 
	for(var i=3; i < max; i++){ //loop through numbers 
		var prime = true; //variable set to true for now 
    for(var x = 2; x < i; x++) //loop through numbers up to the current 'i' number
    {
      if(i % x === 0) //if the first loop number is divisible by the second loop number then it is not prime 
      {
        prime = false;
        
      }
    }
    if(prime == true){
    array.push(i); //if the number is prime, add it to the array
}
  }

	return array; 
}
}




