export default function countNodes(root) {
/*
Modify the contents of 'app/test9.js' and implement a function that counts the nodes in a binary tree
*/
var count =0;
if(root == null)
{
	return count; //if there is no root node then return 0;
}
else if(root.left == null && root.right == null) {
	count = count + 1; //if there is no left or right node then add one for the root node and return a count of 1 
	return count;
}
else{
	count = count + 1 + countNodes(root.left) + countNodes(root.right); //+1 is the root node and then it iterates through the function again each time there is another left or right node and adds them up
	return count;
}
}
