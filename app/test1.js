export function filterNames(names, startsWith){
/* filterNames - should take an array of strings and a search string.
It should return an array of names that startwith the search string */

var finalArray = new Array(); //new array

for(var i = 0; i < names.length; i++) //Loops through array 
{ 
	if(names[i].startsWith(startsWith)) //if the current string starts with the 'startsWith' variable, add it to array
	{
		finalArray.push(names[i]);
	}
}
return finalArray; //return the array 
}

export function objectFilter(objects, key, searchString) {
/* objectFilter - should take an array of objects, a key, and a search string
It should return an array of objects that have a key that is equal to the search string*/ 

var finalArray = new Array(); //new array

for(var i=0; i < objects.length; i++) //loop through objects 
{
	if(objects[i][key] === searchString){ //if the object key = searchstring add to array 
		finalArray.push(objects[i]);
	}
}
return finalArray; //return array 
}

export function compoundObjectFilter(objects, keysString, searchString) {
/* compoundObjectFilter - should take an array of objects, a string with multiple keys seperated by "."s and a search string.
It should return an array of objects that have a key that equals the search string */

var finalArray = new Array();
var split = keysString.split("."); //split the key into strings by the '.'
for(var i = 0; i < objects.length; i++)
{
	if(objects[i][split[0]][split[1]] == searchString) //uses the split string. If there were more than 2 keys in the keystring (eg address.town.street) this would need to be fixed 
	{
		finalArray.push(objects[i]); //add to array if the value matches
	}
}
return finalArray;
}
