export default function test3(numMax){
/*
Modify the contents of 'app/test3.js' and implement a function that will return an array of the fibbonnacci sequence.

https://en.wikipedia.org/wiki/Fibonacci_number.

The function must take an number argument and generate that many numbers for the sequence.
*/
var array = new Array(); //new array

if(numMax === 0 || numMax === undefined)
{
    return array; //if the number is 0 or no number is returned, return blank array
}
if(numMax === 1){
    array[0] = 0;
    return array; //if the number is 1 then populate array with first number and return array
}
if(numMax === 2){  //if the number is 2 then populate array with first 2 numbers and return array
    array[0] = 0; 
    array[1] = 1;
    return array;
}
if(numMax > 2){ //if the number is more than 2, loop through for each number more than 2 and add the two previous numbers together 
    array[0] = 0;
    array[1] = 1;

    for(var i=2; i < numMax; i++) {
        array[i] = array[i-1] + array[i-2];
    }

    return array;
}

}

