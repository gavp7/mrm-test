export function groupBy(people, key) {

}

export function groupBySex(people) {
/*groupBySex - should take an array of people
It should return a new object with keys for the value of sex and values containing array of people with that sex */

var object = { "male": [], //empty object 
			    "female": []
			};

for(var i=0; i < people.length; i++){
	if(people[i]["sex"]=="male"){
		object["male"].push(people[i]); // if the gender is male then add to the males list
	} else {
		object["female"].push(people[i]); //if it is not male then add it to female 
	}
}
return object;
}

export function groupByYearThenSex(people) {
/*
groupByYearThenSex - should take an array of poeple
It shoudl return a new object with keys that are the year the person was born which should have values with keys
for the sex which should have values that are arrays of people born that year with that sex. */

var object = {"1971": {male: [], female: []}, //initial object 
			  "1972": {male: [], female: []}
			  };

for(var i=0; i < people.length; i++){
	if(people[i]["born"] == "1971"){ //if the person was born in 1971 note: I just used 1971 and 72 as after looking at 'people.js' i realised these were the only years, if other years were added this would need to be appended
		if(people[i]["sex"] == "male"){ //if the person is male
			object["1971"]["male"].push(people[i]); 
		} else {
			object["1971"]["female"].push(people[i]); //if the person is female
		}
	}
	else if(people[i]["born"] == 1972){ //if the person was born in 1971
		if(people[i]["sex"] == "male"){  //if the person was male
			object["1972"]["male"].push(people[i]);
		} else { 
			object["1972"]["female"].push(people[i]); //if the person was female
		}
	}
}
return object;


}
