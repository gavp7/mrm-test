export function capitalise(names){
/* capitalise - should take an array of strings
It should return a new array with the strings capitalised */
var strings = new Array();
for( var i = 0; i < names.length; i++){
	strings.push(names[i].toUpperCase()); //convert string to upper case and add to new array
}
return strings; //return new array
}


export function extractValue(objects, key) {
/* extractValue - should take an array of objects and a key
It should return an array of values for the key
*/
var temp = new Array();
for(var i =0; i < objects.length; i++){
	temp.push(objects[i][key]);
}
return temp;
}


export function extractCompoundValue(objects, keysString) {
/* extractCompountValue - should take an array of objects, a string with multiple keys seperated by "."s
It shold return an array of values for the compound key */

var split = keysString.split("."); //split the key into strings by the '.' 
var temp = new Array();

for(var i=0; i < objects.length; i++)
{
	temp.push(objects[i][split[0]][split[1]]); //similar to test 1, if there were more than 1 '.' in the string then this method would not work 
}
return temp;
}
